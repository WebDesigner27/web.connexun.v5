<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <base href="/">
  <title>Behind APIs - Connexun</title>
  <!-- Stylesheets -->
  <link href="assets/css/main.css" rel="stylesheet">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
  <!-- Responsive -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="assets/js/respond.js"></script><![endif]-->
</head>

<body class="transparent-header">

  <!-- Preloader -->
  <div class="preloader">
    <!-- Google Chrome -->
    <div class="infinityChrome">
      <div></div>
      <div></div>
      <div></div>
    </div>
    <!-- Safari and others -->
    <div class="infinity">
      <div><span></span></div>
      <div><span></span></div>
      <div><span></span></div>
    </div>
  </div>

  <?php include('../inc_header_nav.php'); ?>

  <!-- Body contents goes here -->
  <section class="heroheader">
    <div class="auto-container">
      <div class="text-container col-lg-8 col-md-8 col-sm-12">
        <h2>Behind APIs</h2>
        <h1>Title goes here...</h1>
        <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
          Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
      </div>
      <div class="cover-image">
        <img src="assets/images/demo/ai.svg">
      </div>
    </div>
  </section>

  <section>
    <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
      <h2 class="section__title">Our Solutions</h2>
      <p class="section__description">Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
        Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
    </div>
  </section>

  <section>
    <div class="auto-container rounded-colored wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
      <div class="row d-flex h-100">
        <div class="col-lg-6 col-md-6 col-sm-12 justify-content-center align-self-center">
          <h2>Real-time News Tracking</h2>
          <p>Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
            <a href="real-time-news-traking" class="theme-btn btn-style-three btn-red"><span class="txt">Learn More</span></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <img src="assets/images/demo/data-tracking.svg">
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="auto-container rounded-colored wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
      <div class="row d-flex h-100">
        <div class="col-lg-6 col-md-6 col-sm-12 justify-content-center align-self-center">
          <h2>AI-Driven Media Intelligence</h2>
          <p>Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
            <a href="ai-driven-media-intelligence" class="theme-btn btn-style-three btn-red"><span class="txt">Learn More</span></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <img src="assets/images/demo/ai-hands.svg">
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="auto-container rounded-colored wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
      <div class="row d-flex h-100">
        <div class="col-lg-6 col-md-6 col-sm-12 justify-content-center align-self-center">
          <h2>Natural Language Processing (NLP)</h2>
          <p>Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
            <a href="natural-language-processing-nlp" class="theme-btn btn-style-three btn-red"><span class="txt">Learn More</span></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <img src="assets/images/demo/data-processing.svg">
        </div>
      </div>
    </div>
  </section>

  <?php include('../inc_footer.php'); ?>

  <!-- Js Scripts START -->
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/codecarousel.js"></script>
  <script src="assets/js/owl.js"></script>
  <script src="assets/js/jquery.jsonview.min.js"></script>
  <script src="assets/js/jquery.background-video.js"></script>
  <script src="assets/js/scripts.js"></script>
  <!-- Js Scripts END -->
</html>
</body>
