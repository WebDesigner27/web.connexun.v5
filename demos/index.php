<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>API Demos - Connexun</title>
  <!-- Stylesheets -->
  <link href="https://www.connexun.com/demo2/assets/css/main.css" rel="stylesheet">
  <!-- Favicon -->
  <link rel="shortcut icon" href="https://www.connexun.com/demo2/assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="https://www.connexun.com/demo2/assets/images/favicon.png" type="image/x-icon">
  <!-- Responsive -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="https://www.connexun.com/demo2/assets/js/respond.js"></script><![endif]-->
</head>

<body>

  <!-- Preloader -->
  <div class="preloader">
    <!-- Google Chrome -->
    <div class="infinityChrome">
      <div></div>
      <div></div>
      <div></div>
    </div>
    <!-- Safari and others -->
    <div class="infinity">
      <div><span></span></div>
      <div><span></span></div>
      <div><span></span></div>
    </div>
  </div>

  <?php echo file_get_contents('https://www.connexun.com/demo2/inc_header_nav.php'); ?>

  <!-- Body contents goes here -->
  <section id="demos">
    <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
      API Demos page
    </div>
  </section>

  <?php echo file_get_contents('https://www.connexun.com/demo2/inc_footer.php'); ?>

  <!-- Js Scripts START -->
  <script src="https://www.connexun.com/demo2/assets/js/jquery.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/bootstrap.min.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/wow.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/owl.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/codecarousel.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/jquery.jsonview.min.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/jquery.background-video.js"></script>
  <script src="https://www.connexun.com/demo2/assets/js/scripts.js"></script>
  <!-- Js Scripts END -->
</body>
</html>
