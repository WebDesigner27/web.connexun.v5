<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Connexun</title>
  <!-- Stylesheets -->
  <link href="assets/css/main.css" rel="stylesheet">
  <link href="assets/css/index.css" rel="stylesheet">
  <link href="assets/css/herovideo.css" rel="stylesheet">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
  <!-- Responsive -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="assets/js/respond.js"></script><![endif]-->
</head>

<body class="transparent-header">

  <!-- Preloader -->
  <div class="preloader">
    <!-- Google Chrome -->
    <div class="infinityChrome">
      <div></div>
      <div></div>
      <div></div>
    </div>
    <!-- Safari and others -->
    <div class="infinity">
      <div><span></span></div>
      <div><span></span></div>
      <div><span></span></div>
    </div>
  </div>

  <?php include('inc_header_nav.php'); ?>

  <!-- Body contents goes here -->
  <div class="video-bg jquery-background-video-wrapper">
    <div class="hero-text">
      <div class="auto-container">
        <h1 class="wow fadeInLeft" data-wow-delay="2500ms" data-wow-duration="1000ms">The Ultimate AI News Intelligence Engine</h1>
        <p class="wow fadeInLeft" data-wow-delay="2500ms" data-wow-duration="1000ms">Source real time multilingual <strong>headlines, articles &amp; dynamic summaries</strong> from over 20.000 trusted information sites with our news &amp; information
          API.</p>
        <div class="row">
          <div class="intelligence-block col-lg-4 col-md-4 col-sm-12">
            <div class="inner-box wow fadeInLeft" data-wow-delay="3000ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
              <div class="icon-box">
                <img src="assets/images/icons/enterprises-agencies.svg" alt="Enterprises, Agencies, Businesses">
              </div>
              <h5>For Enterprises &amp; Agencies</h5>
              <div class="text">
                <p>Ideal for PR, marketing, media monitoring, media intelligence and data-journalism agencies, seeking to turn raw news content into multipurpose actionable data.</p>
              </div>
            </div>
          </div>
          <div class="intelligence-block col-lg-4 col-md-4 col-sm-12">
            <div class="inner-box wow fadeInLeft" data-wow-delay="3500ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 150ms; animation-name: fadeInLeft;">
              <div class="icon-box">
                <img src="assets/images/icons/start-up.svg" alt="Startups &amp; SMEs">
              </div>
              <h5>For Startups &amp; SMEs</h5>
              <div class="text">
                <p>Ideal for businesses building their own application or website or seeking to scrape websites for highly relevant and reliable news content.</p>
              </div>
            </div>
          </div>
          <div class="intelligence-block col-lg-4 col-md-4 col-sm-12">
            <div class="inner-box wow fadeInLeft" data-wow-delay="4000ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 150ms; animation-name: fadeInLeft;">
              <div class="icon-box">
                <img src="assets/images/icons/developers.svg" alt="Free for Coders and Developers">
              </div>
              <h5>Free For Developers</h5>
              <div class="text">
                <p>Jump right in if you're in development or open source. Check out our very generous free plan.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="wow fadeInDown" data-wow-delay="5000ms" data-wow-duration="1000ms">
          <a href="#meet-birbal" class="scroll-to-target theme-btn btn-style-three btn-white"><span class="txt">How it works</span></a>&nbsp;&nbsp;&nbsp;<a href="#request-demo" class="theme-btn btn-style-three btn-red"><span class="txt">Start a Free Trial</span></a>
        </div>
      </div>
    </div>
    <video class="background-video jquery-background-video" loop autoplay muted playsinline poster="https://ak3.picdn.net/shutterstock/videos/9166553/thumb/12.jpg">
      <source src="https://ak3.picdn.net/shutterstock/videos/9166553/preview/stock-footage-growing-connections-over-the-earth-a-worldwide-network-expanding-over-the-world-loops-seamlessly.webm" type="video/webm">
      <source src="https://ak3.picdn.net/shutterstock/videos/9166553/preview/stock-footage-growing-connections-over-the-earth-a-worldwide-network-expanding-over-the-world-loops-seamlessly.mp4" type="video/mp4">
    </video>
  </div>

  <section id="main-animation">
    <div class="inner-vertical-centered">
      <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
        <h2 class="section__title">Meet B.I.R.B.AL.</h2>
        <p class="section__description">Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
          Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
      </div>
      <div class="auto-container">SVG animation goes here<br>Illustration about our aggregation process.</div>
    </div>
  </section>

  <section>
    <div class="inner-vertical-centered">
      <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
        <h2 class="section__title">Advanced web scraper & classifying technology at your fingertips</h2>
        <p class="section__description">Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
          Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
      </div>
      <div class="auto-container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
          </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
              Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="inner-vertical-centered">
      <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
        <h2 class="section__title">What makes us different?</h2>
        <p class="section__description">Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
          Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
      </div>
      <div class="auto-container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
          </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
              Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p>Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
            Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="inner-vertical-centered">
    <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
      <h2 class="section__title">Test our Technology</h2>
      <p class="section__description">Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
        Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
    </div>
    <div class="auto-container">Horizontal slider and call to action buttons.</div>
    </div>
  </section>

  <section>
    <div class="inner-vertical-centered">
      <div class="auto-container wow slideInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
        <h2 class="section__title">APIs Showcase</h2>
        <p class="section__description">Vivamus consectetur commodo urna, at tincidunt eros pellentesque sed. Sed sed ullamcorper nibh. Donec auctor tortor purus, quis efficitur dui efficitur ut. Donec fermentum rutrum nisl quis mollis. In vel arcu arcu.
          Cras sit amet orci vel ex facilisis ullamcorper. Morbi fermentum turpis quam, id vestibulum elit semper sed. Aenean eu tincidunt turpis.</p>
      </div>
      <div class="auto-container">Horizontal slider of API's pre-built calls JSON responses.</div>
    </div>
  </section>

  <?php include('inc_footer.php'); ?>

  <!-- Js Scripts START -->
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/owl.js"></script>
  <script src="assets/js/codecarousel.js"></script>
  <script src="assets/js/jquery.jsonview.min.js"></script>
  <script src="assets/js/jquery.background-video.js"></script>
  <script src="assets/js/scripts.js"></script>
  <!-- Js Scripts END -->
</body>
</html>
