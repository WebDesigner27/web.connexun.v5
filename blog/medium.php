<?php
$xml_doc=file_get_contents('https://medium.com/feed/@connexun');
$xml = simplexml_load_string($xml_doc);
preg_match_all('/(<content:encoded><!\[CDATA\[.*?]]><\/content:encoded>)/',$xml_doc,$contents_matches);
// print_r($contents_matches);
$counter=0;
foreach ($xml->channel->item as $item){
  ?>
  <div class="article-item">
    <div class="article-header">
      <img src="https://www.connexun.com/demo2/assets/images/favicon.png" width="16" height="16">
      <a target="_blank" class="author" href="https://medium.com/@connexun">Connexun | news api</a>
      <span>&nbsp;@&nbsp;<img src="https://cdn4.iconfinder.com/data/icons/social-media-2210/24/Medium-16.png" width="16" height="16">&nbsp;<span>Medium</span></span>
    </div>
    <div class="article-image">
      <a target="_blank" href="<?php echo explode('?',$item->link)[0]; ?>">
        <?php
          preg_match_all('/(<figure>.*?<\/figure>)/',$contents_matches[0][$counter],$image_match);
          preg_match_all('/(src=".*?")/',$image_match[0][0],$article_image);
          preg_match_all('/(alt=".*?")/',$image_match[0][0],$article_image_title);
        ?>
        <img class="art-bd-img" <?php echo $article_image[0][0]; ?> <?php echo $article_image_title[0][0]; ?> style="opacity: 1;">
      </a>
    </div>
    <div class="article-body">
      <h1 class="title">
        <a target="_blank" href="<?php echo explode('?',$item->link)[0]; ?>"><?php echo $item->title; ?></a>
      </h1>
      <p class="tags"><?php foreach ($item->category as $category) echo '<a href="https://medium.com/tag/'.$category.'" target="_blank">#'.ucwords(str_replace('-',' ',$category)).' '; ?></p>
      <a class="action" target="_blank" href="<?php echo explode('?',$item->link)[0]; ?>">Leggi l'articolo su medium.com &gt;</a>
    </div>
  </div>
<?php $counter=$counter+1; } ?>
