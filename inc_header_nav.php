<header>
  <nav class="auto-container" role="navigation">
    <a class="logo wow fadeInDown" data-wow-delay="1500ms" data-wow-duration="500ms" href="/"><img src="https://www.connexun.com/demo2/assets/images/logo_connexun.svg" alt="Connexun"></a>
    <ul class="d-none d-lg-block">
      <li class="wow fadeInDown dropdown" data-wow-delay="1600ms" data-wow-duration="500ms">
        <a href="behind-apis">Behind APIs</a>
        <span class="dropdown-menu">
          <span class="dropdown-section">
            <nav class="menu">
              <a href="real-time-news-tracking">
                <span class="title">Real-time News Tracking</span>
                <span class="sub-title">Webpage crawler scraping news content published by over 20.000 highly trusted sources.</span></a>
              <a href="ai-driven-media-intelligence">
                <span class="title">AI-Driven Media Intelligence</span>
                <span class="sub-title">Make use of B.I.R.B.AL., our artificial intelligent engine that uses probabilistic models and news image analysis to spot premium content, detect and omit fake news, duplicates and unreliable content, and more, amongst scraped data.</span>
              </a>
              <a href="natural-language-processing-nlp">
                <span class="title">Natural Language Processing (NLP)</span>
                <span class="sub-title">B.I.R.B.AL. uses supervised and unsupervised machine learning algorithms trained by employing a database with over a million articles in different languages, applying state of the art models of Natural Language Processing.</span>
              </a>
            </nav>
          </span>
        </span>
      </li>
      <li class="wow fadeInDown dropdown" data-wow-delay="1700ms" data-wow-duration="500ms">
        <a href="products">Products</a>
        <span class="dropdown-menu">
          <span class="dropdown-section">
            <nav class="menu">
              <a href="news-api">
                <span class="title">News API</span>
                <span class="sub-title">News from around the world, which speak or come from a particular country or inherent in the intersection between two distinct countries.</span></a>
              <a href="nationality-api">
                <span class="title">Nationality API</span>
                <span class="sub-title">Prediction on the origin of a specific term or phrase.</span>
              </a>
              <a href="summarize-api">
                <span class="title">Summarize API</span>
                <span class="sub-title">Summary process based on language and chars amount.</span>
              </a>
              <a href="relation-index-statistics-api">
                <span class="title">Relation Index Statistics API</span>
                <span class="sub-title">Frequency at which one country’s sources mention another country in any period of time.</span>
              </a>
              <a href="minority-report-api">
                <span class="title">Minority Report API</span>
                <span class="sub-title">Statistics and forecasts regarding multi-ethnicity and / or a specific ethnicity for each Italian municipality.</span>
              </a>
              <a href="https://demos.connexun.com/">
                <span class="title">Demos</span>
              </a>
              <a href="https://docs.connexun.com/">
                <span class="title">Documentation</span>
              </a>
            </nav>
          </span>
        </span>
      </li>
      <li class="wow fadeInDown" data-wow-delay="1800ms" data-wow-duration="500ms"><a href="https://blog.connexun.com/">Blog</a></li>
      <li class="wow fadeInDown dropdown" data-wow-delay="1900ms" data-wow-duration="500ms">
        <a href="#" onclick="return false;">Company</a>
        <span class="dropdown-menu">
          <span class="dropdown-section">
            <nav class="menu">
              <a href="about-us">
                <span class="title">About Us</span>
                <span class="sub-title">Morbi fermentum turpis quam, id vestibulum elit semper sed tincidunt.</span></a>
              <a href="research">
                <span class="title">Research</span>
                <span class="sub-title">Morbi fermentum turpis quam, id vestibulum elit semper sed tincidunt.</span>
              </a>
              <a href="https://app.connexun.com/">
                <span class="title">Test our Mobile App</span>
                <span class="sub-title">Morbi fermentum turpis quam, id vestibulum elit semper sed tincidunt.</span>
              </a>
            </nav>
          </span>
        </span>
      </li>
      <li class="wow fadeInDown" data-wow-delay="2000ms" data-wow-duration="500ms"><a href="contacts">Contacts</a></li>
    </ul>
  </nav>
</header>
<nav class="d-lg-none" role="mobile-navigation">
  <div id="menuToggle">
    <input type="checkbox" />
    <span></span>
    <span></span>
    <span></span>
    <ul id="menu">
      <li>
        <strong>Behind APIs</strong>
        <ul>
          <li>
            <a href="real-time-news-tracking">Real-time News Tracking</a>
            <div>Webpage crawler scraping news content published by over 20.000 highly trusted sources.</div>
          </li>
          <li>
            <a href="ai-driven-media-intelligence">AI-Driven Media Intelligence</a>
            <div>Make use of B.I.R.B.AL., our artificial intelligent engine that uses probabilistic models and news image analysis to spot premium content, detect and omit fake news, duplicates and unreliable content, and more, amongst scraped data.</div>
          </li>
          <li>
            <a href="natural-language-processing-nlp">Natural Language Processing (NLP)</a>
            <div>B.I.R.B.AL. uses supervised and unsupervised machine learning algorithms trained by employing a database with over a million articles in different languages, applying state of the art models of Natural Language Processing.</div>
          </li>
        </ul>
      </li>
      <li>
        <strong>Products</strong>
        <ul>
          <li>
            <a href="news-api">News API</a>
            <div>News from around the world, which speak or come from a particular country or inherent in the intersection between two distinct countries.</div>
          </li>
          <li>
            <a href="nationality-api">Nationality API</a>
            <div>Prediction on the origin of a specific term or phrase.</div>
          </li>
          <li>
            <a href="summarize-api">Summarize API</a>
            <div>Summary process based on language and chars amount.</div>
          </li>
          <li>
            <a href="relation-index-statistics-api">Relation Index Statistics API</a>
            <div>Frequency at which one country’s sources mention another country in any period of time.</div>
          </li>
          <li>
            <a href="minority-report-api">Minority Report API</a>
            <div>Statistics and forecasts regarding multi-ethnicity and / or a specific ethnicity for each Italian municipality.</div>
          </li>
          <li><a href="https://demos.connexun.com/">Demos</a></li>
          <li><a href="https://docs.connexun.com/">Documentation</a></li>
        </ul>
      </li>
      <li><a href="https://blog.connexun.com/">Blog</a></li>
      <li>
        <strong>Company</strong>
        <ul>
          <li>
            <a href="about-us">About Us</a>
            <div>Morbi fermentum turpis quam, id vestibulum elit semper sed tincidunt.</div>
          </li>
          <li>
            <a href="research">Research</a>
            <div>Morbi fermentum turpis quam, id vestibulum elit semper sed tincidunt.</div>
          </li>
          <li>
            <a href="https://app.connexun.com/">Test our Mobile App</a>
            <div>Morbi fermentum turpis quam, id vestibulum elit semper sed tincidunt.</div>
          </li>
        </ul>
      </li>
      <li><a href="contacts">Contacts</a></li>
    </ul>
  </div>
</nav>
