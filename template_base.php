<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Connexun</title>
  <!-- Stylesheets -->
  <link href="assets/css/main.css" rel="stylesheet">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
  <!-- Responsive -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="assets/js/respond.js"></script><![endif]-->
</head>

<body>

  <!-- Preloader -->
  <div class="preloader">
    <!-- Google Chrome -->
    <div class="infinityChrome">
      <div></div>
      <div></div>
      <div></div>
    </div>
    <!-- Safari and others -->
    <div class="infinity">
      <div><span></span></div>
      <div><span></span></div>
      <div><span></span></div>
    </div>
  </div>

  <?php include('inc_header_nav.php'); ?>

  <!-- Body contents goes here -->

  <?php include('inc_footer.php'); ?>

  <!-- Js Scripts START -->
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/owl.js"></script>
  <script src="assets/js/codecarousel.js"></script>
  <script src="assets/js/jquery.jsonview.min.js"></script>
  <script src="assets/js/jquery.background-video.js"></script>
  <script src="assets/js/scripts.js"></script>
  <!-- Js Scripts END -->
</body>
</html>
