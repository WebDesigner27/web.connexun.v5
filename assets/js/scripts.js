(function($) {

  "use strict";

  // Preloder browser based
  var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
  if (!isChrome) {
    $('.infinityChrome').css('display', 'none');
    $('.infinity').css('display', 'block');
  }

  //Hide Loading Box (Preloader)
  function handlePreloader(){
    if ($('.preloader').length) {
      $('.preloader').delay(2000).fadeOut(500);

      setTimeout(function(){
        // Hero Video background
        $('.background-video').bgVideo({ fadeIn: 500 });
        // Body Overflow to Auto
        $('body').css('overflow-y','auto');
      },2500);

      // Elements Animation
      setTimeout(function(){
        if ($('.wow').length) {
          var wow = new WOW({
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: true, // trigger animations on mobile devices (default is true)
            live: true // act on asynchronously loaded content (default is true)
          });
          wow.init();
        }
      },1000);

    }
  }


  // Mobile Nav menu
  $('#menuToggle input').on('click',function(){
    if($(this).is(':checked')) $('body').css('overflow-y','hidden');
    else {
      $('#menu strong').removeClass('opened');
      $('#menu li ul').removeClass('opened');
      $('body').css('overflow-y','auto');
    }
  });
  $('#menu strong').click(function(e) {
    e.preventDefault();
    $(this).toggleClass('opened');
    $(this).parent().find('ul').toggleClass('opened');
  });


  //Update Header Style and Scroll to Top
  function headerStyle() {
    if ($('header').length) {
      var windowpos = $(window).scrollTop();
      // var siteHeader = $('.main-header');
      // var scrollLink = $('.scroll-to-top');
      var sticky_header = $('header');
      if (windowpos > 100) {
        $('body').addClass('sticky-header');
        sticky_header.addClass("animated slideInDown");
        //scrollLink.fadeIn(300);
      } else {
        $('body').removeClass('sticky-header');
        sticky_header.removeClass("animated slideInDown");
        // scrollLink.fadeOut(300);
      }
    }
  }

  headerStyle();


  // Scroll to a Specific Div
  if ($('.scroll-to-target').length) {
    $(".scroll-to-target").on('click', function() {
      var target = $(this).attr('data-target');
      // animate
      $('html, body').animate({
        scrollTop: $(target).offset().top
      }, 1500);

    });
  }


  // Code Carousel
  if ($('.code-carousel').length) {
    $('.code-carousel').codeCarousel();
  }


  // Accordion
  if ($('.accordion').length) {
    $('.accordion').find('.accordion-title').on('click', function() {
      // Adds Active Class
      $(this).toggleClass('active');
      // Expand or Collapse This Panel
      $(this).next().slideToggle('fast');
      // Hide The Other Panels
      $('.accordion-content').not($(this).next()).slideUp('fast');
      // Removes Active Class From Other Titles
      $('.accordion-title').not($(this)).removeClass('active');
    });
  }


  // On Scroll functions
  $(window).on('scroll', function() {
  	headerStyle();
  });


  // OnLoad functions
  $(window).on('load', function() {
    handlePreloader();
  });

})(window.jQuery);
