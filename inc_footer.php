<footer>
  <div class="auto-container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <h5>Lorem Ipsum</h5>
        <ul>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <h5>Lorem Ipsum</h5>
        <ul>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <h5>Lorem Ipsum</h5>
        <ul>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <h5>Lorem Ipsum</h5>
        <ul>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
          <li><a href="#">Lorem ipsum sit</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="text-center">
    <p>© 2020 <strong>Connexun</strong> S.r.l. All rights reserved</p>
  </div>
</footer>
